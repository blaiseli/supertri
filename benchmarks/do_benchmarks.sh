#!/bin/bash
# Usage: ./do_benchmarks.sh bootstrap
# Usage: ./do_benchmarks.sh posterior

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


[ ${1} ] && support_type=${1} || support_type="bootstrap"
# support_type="posterior"

case ${support_type} in
    "bootstrap")
        suffix=".log"
        intrees="--intree ../test_data/synthesistree.tre"
        test_data_dir="../test_data/bootstrap_test_data"
        taxa_file="${test_data_dir}/taxons"
        gene_file="${test_data_dir}/marqueurs"
        root="Antilocapraamericana"
        wtset_name="Cumulated_bootstrap_proportions"
        do_compare="ref"
        ;;
    "posterior")
        suffix=".parts"
        intrees=""
        test_data_dir="../test_data/newformat_test_data"
        taxa_file="${test_data_dir}/taxa.txt"
        gene_file="${test_data_dir}/genes.txt"
        root="T000"
        wtset_name="Cumulated_posterior_probabilities"
        do_compare=""
        ;;
    *) error_exit "Unknown support type: ${support_type}";;
esac

for py_version in "2.7" "3.6"
do
    echo "Running supertri.py with python${py_version}"
    python${py_version} -m cProfile -o profile_${support_type}_${py_version} ../supertri.py \
            --datasets ${gene_file} \
            --indir ${test_data_dir} \
            ${intrees} \
            -o ${support_type}_test_supermatrix.nex \
            --outdir . \
            --suffix ${suffix} \
            --taxlist ${taxa_file} \
            --root ${root} \
            > log_${support_type}_${py_version} \
            2> err_${support_type}_${py_version} || error_exit "supertri.py failed for ${support_type} and ${py_version}"
    if [ "${intrees}" ]
    then
        mv synthesistree.tre.withindices ${support_type}_test_synthesistree_${py_version}.tre
    fi
    mv ${support_type}_test_supermatrix.nex ${support_type}_test_supermatrix_${py_version}.nex
done

