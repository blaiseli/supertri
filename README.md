Short description:
-----------------

`supertri.py` is a python script that will help you producing a matrix
representation weighted by bootstrap proportions or Bayesian posterior
probabilities. Such a matrix representation can be used to produce supertrees.
`supertri.py` also computes the support indices described in Anne Ropiquet's
thesis: reproducibility and mean robustness (or mean Bayesian posterior
probability) and may report these indices on nexus trees.


Citing
------

If you use this program, please cite Ropiquet _et al._ (2009), SuperTRI: A new
approach based on branch support analyses of multiple independent data sets for
assessing reliability of phylogenetic inferences

URL: <http://dx.doi.org/10.1016/j.crvi.2009.05.001>.


Copyright notice (see also the file `COPYING.txt`):
----------------

Copyright (C) 2007-2018 Anne Ropiquet, Blaise Li and Alexandre Hassanin

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


## How to install `supertri.py`?


### A) You need python

`supertri.py` is a python script. To be able to run it, you must first check
whether python is installed.

Open a command-line terminal and type `python`. You'll have to use
command-line many times, but don't worry, the basic operations are simple once
you got used to it. Commands usually consist in the name of the command,
optionally followed by options and arguments separated by blank spaces. To
validate a command, press the enter key. In MacOSX there is a command-line
terminal called "Terminal"; it should be in the "Utilities" directory of the
"Applications" directory.

#### 1) Favourable case:

If python is installed, you should get something like that:
```
Python 2.7.13 (default, Sep 26 2018, 18:42:22)
[GCC 6.3.0 20170516] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

The interactive python interpreter is ready to accept commands. You can use it
as a calculator if you wish but beware of the division; if you provide only
integers, the result will be an integer. The first line gives the version
number (here 2.7.13): it must be at least version 2.6, and less than 3.0. If
not, go to <http://www.python.org/> and get a suitable version. You will perhaps
have to restart the terminal in order to take the change into account.

Once you've played enough with the python interpreter to see that it works,
press ctrl-D. This will send an "end-of-file" signal to the python interactive
interpreter. This is the way to make it quit. You can now go to B). If you want
to learn python in order to be able to modify `supertri.py` to suit your specific
needs, search a tutorial on the web.


#### 2) Less favourable case

If you get something like `python: command not found`, either python is not
installed, either the program is somewhere in your computer but not in a place
where the commands are usually looked for (the PATH).

If you think python is installed, try to locate it and to add the good
location to your PATH (see <http://en.wikipedia.org/wiki/Path_(computing)>,
<http://fr.wikipedia.org/wiki/PATH> (if you can read french) and ask google for
further help).

If you think python is really not installed, install it (see
<http://www.python.org/>, take the last available version before version 3) and
go back to A). Python is available for a wide range of computers, and it's free
(and rather easy to learn, for a programming language).


### B) Choose a place where to install supertri.py

It is advisable to put files in a place where you you can find it later. There
are usually some conventions on where to put executable files. For example, on
an UNIX-like operating system (UNIX, Linux, MacOSX), executables are often
placed in directories ending in "bin" (bin for "binary": that is, programs
compiled in machine language, note that `supertri.py` is not in machine
language; you may open this python script in a text editor and see
human-readable commands, python is the program that will translate these
commands into machine language).

There are system-wide bin directories, like `/usr/local/bin`, that are usually in
the PATH of every user (this means that programs in such a place will be found
and executed when the user types the name of the program in a command-line
terminal). To place a program in `/usr/local/bin` you need to have administrative
rights. Under MacOSX, it is possible that directories like `/usr/local/bin` are
not visible from the finder, but only from the command-line terminal. In such a
case, you will have to use the command `cp` to copy `supertri.py` to the place
you want.

If you are not allowed to place a program in `/usr/local/bin`, you may place the
program in a personal bin directory. A good place would be a bin directory in
your home directory. For example, if your username is `dupont` and you are in a
MacOSX system, this would be `/Users/dupont/bin`. On a Linux system, this would
be `/home/dupont/bin`.


### C) There are several ways to execute a python script

Once you have placed `supertri.py` in a suitable place, if you want to be able
to call the command directly, you'll have to check wether that place is in
your PATH or not (see A) 2)). You may also call python and tell it to run the
`supertri.py` script.


#### 1) The directory containing `supertri.py` is in your PATH

This is probably the most convenient situation.

Open a command-line terminal.

Go to the directory where you placed `supertri.py`, using the command `cd`
("change directory"):

MacOSX-like example:
```
Welcome to Darwin !
Computer:~ dupont$ cd /Users/dupont/bin
Computer:~/bin dupont$
```

Make the script executable, using the command `chmod`:
```
Computer:~/bin dupont$ chmod +x supertri.py
Computer:~/bin dupont$
```

Now you can call `supertri.py` in command-line from any directory (provided your
PATH contains the directory where `supertri.py` has been placed):

```
Computer:~/bin dupont$ cd
Computer:~ dupont$ supertri.py

SuperTRI
Revision: 59

A python script to help assembling supertree matrices with clade weights

Distributed under the GNU General Public Licence


usage: supertri.py [options] <arguments>
available options:
[list of options here]

Traceback (most recent call last):
  File "supertri.py", line 1284, in <module>
    sys.exit(main())
  File "supertri.py", line 1014, in main
    raise Exception("No dataset names provided. Provide a file with option --datasets.")
Exception: No dataset names provided. Provide a file with option --datasets.
```

Well, it seems to work. Go to D).


#### 2) You want to call python explicitly

Open a command-line terminal.

Go to the directory where you placed `supertri.py`, using the command `cd`:

MacOSX-like example:
```
Welcome to Darwin !
Computer:~ dupont$ cd /Users/dupont/bin
Computer:~/bin dupont$
```

Tell python to execute the script:
```
Computer:~/bin dupont$ python supertri.py

SuperTRI
Revision: 59

A python script to help assembling supertree matrices with clade weights

Distributed under the GNU General Public Licence


usage: supertri.py [options] <arguments>
available options:
[list of options here]

Traceback (most recent call last):
  File "supertri.py", line 1284, in <module>
    sys.exit(main())
  File "supertri.py", line 1014, in main
    raise Exception("No dataset names provided. Provide a file with option --datasets.")
Exception: No dataset names provided. Provide a file with option --datasets.
```

Well, it seems to work. Go to D).


#### 3) You want to execute the script from anywhere, but it is not in your PATH

Open a command-line terminal.

Go to the directory where you placed `supertri.py`, using the command `cd`:

MacOSX-like example:
```
Welcome to Darwin !
Computer:~ dupont$ cd /Users/dupont/bin
Computer:~/bin dupont$
```

Make the script executable, using the command `chmod`:
```
Computer:~/bin dupont$ chmod +x supertri.py
Computer:~/bin dupont$
```

Go to the directory where you placed your analyses results, using the command `cd`:
```
Computer:~/bin dupont$ cd /Users/dupont/Documents/Analyses/Smurfs/bootstrap
Computer:~/Documents/Analyses/Smurfs/bootstrap dupont$
```

Execute the script, calling it with its absolute path:
```
Computer:~/Documents/Analyses/Smurfs/bootstrap dupont$ /Users/dupont/bin/supertri.py
Computer:~/bin dupont$ python supertri.py

SuperTRI
Revision: 59

A python script to help assembling supertree matrices with clade weights

Distributed under the GNU General Public Licence


usage: supertri.py [options] <arguments>
available options:
[list of options here]

Traceback (most recent call last):
  File "/Users/dupont/bin/supertri.py", line 995, in ?
    main()
  File "/Users/dupont/bin/supertri.py", line 788, in main
    raise Exception("No dataset names provided. Provide a file with option --datasets.")
Exception: No dataset names provided. Provide a file with option --datasets.
```

Well, it seems to work. Go to D).

How to use `supertri.py` ?


### D) Prepare the necessary files

A preliminary piece of advice:

When working with command-line tools, it is usually better not to have files
with names containing spaces, accentuated letters or special characters other
than underscore (`_`). Spaces are used to separate commands and arguments.
Special characters may have special meanings. Accentuated letters are not
always handled correctly.

To produce a valid nexus matrix representation of your results, weighted by
support values, `supertri.py` needs at least the following:
- files describing the bipartitions with `.` and `*` and the support values,
  produced by the analyses of your different datasets.
- a file containing the taxon names, in the same order as in the matrix used
  for the analyses, without blanks in the names, and with a name on each line
  of the file.
- if there are taxa missing from one or more datasets, prepare for each
  dataset a file containing the names of the taxa missing for this dataset.

The results files should be named the following way:
Dataset.parts for the results of a MrBayes analysis, where Dataset is the name
of the dataset that was analysed.
Dataset.log for the log of a PAUP bootstrap analysis, where Dataset is the
name of the dataset that was analysed.

Since the description of bipartitions with `.` and `*` is made without taxon
names, the analyses should all have been done with the taxa in the same order
in the matrix. Otherwise, the representations of the bipartitions from two
different analyses will not be comparable. This order is given to `supertri.py`
by the file with the names of the taxa.

The files for the missing taxa should contain the taxa names as they appear in
the list of the taxa. These files should be named the following way:
Dataset.abs, where Dataset is the name of the dataset lacking the taxa in the file.
They should be in the same directory as the files describing the bipartitions.

It is recommended, but optional to prepare a file with the names of the
datasets, without blanks in the names of the datasets, and with one name per
line. These names should correspond to the prefixes of the files containing
the results.

It is advisable not to have other files than those concerned with the analyses
in the directory where you put the data for `supertri.py`.


### E) Executing `supertri.py`

As many command-line tools, `supertri.py` works with a system of options.

Suppose the names of the taxa are written in a file named "taxons". To
indicate `supertri.py` to use this file, the command will be called with the
`--taxlist` option:
```
Computer:~ dupont$ supertri.py --taxlist taxons
```

This is not enough to make it work. If you want to provide a list of datasets
on the command line, with a file named "marqueurs", you will have to use the
`--datasets` option:
```
Computer:~ dupont$ supertri.py --taxlist taxons --datasets marqueurs
```

You may also specify the type of results files with the `--suffix` option:
```
Computer:~ dupont$ supertri.py --taxlist taxons --datasets marqueurs --suffix .log
```

By default, the program will look for bipartitions files in the directory from
where it was started, whose names start with the dataset names, and ending with
the above suffix. You can set a different directory using the `--indir` option.


You can choose the name of the matrix to write with the option `-o` (for **o**utput):
```
Computer:~ dupont$ supertri.py --taxlist taxons --datasets marqueurs --suffix .log -o MRP.nex
```

The default name will be composed by the names of the datasets followed by
".mrpp.nex" for MrBayes data or ".mrbp.nex" for bootstrap log data.
You can chose the root of the output trees with the option --root:
```
Computer:~ dupont$ supertri.py --taxlist taxons --datasets marqueurs --suffix .log -o MRP.nex --root The_outgroup_taxon
```

You must ensure that the name you give is present in the file listing the
taxa, but not in any .abs file.

You can give trees on which to map the indices with the option `--intree`:
```
Computer:~ dupont$ supertri.py --taxlist taxons --datasets marqueurs --suffix .log -o MRP.nex --root The_outgroup_taxon --intree synthesistree.tre
```

The trees you give with this option must be in nexus format and contain the
names of the taxa in the parenthesized representation of the tree; not in a
translation block. You can provide several trees in the same file, or call the
option several times with different file names.

If you want to map the indices on a tree obtained from the matrix
representation produced by a first run of `supertri.py`, you'll have to run
`supertri.py` again, with the `--intree` option, with the previously used
parameters.

The indices will be written as node labels in 2 separate nexus trees.


### F) Miscellaneous remarks

This program was not written by a professional programmer. We hope it works
and we hope the code is clear enough to allow you to modify it if you know
python.

If python works and you are able to launch `supertri.py` but you still have
problems running this program, check carefully that your files conform to the
requirements of part D).

There may also be problems due to file end-of-line coding.

Before reporting bugs, note the error messages, if any, and try to also
determine the version of python and the version of `supertri.py` that you are
using. Please also indicate the type of operating system you are using
(Windows, Mac, Linux, with details about the particular version of the
operating system). This program has not been tested on Windows.
