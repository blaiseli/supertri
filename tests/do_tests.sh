#!/bin/bash
# Usage: COMMIT="064ba82dfa2fdf5054d002416f43d61471bb4565" ./do_tests.sh bootstrap
# Usage: COMMIT="064ba82dfa2fdf5054d002416f43d61471bb4565" ./do_tests.sh posterior

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}


current_commit=$(git log | head -1 | cut -d " " -f 2)
# reference commit can (and should usually) be forced using COMMIT environment variable
[ ${COMMIT} ] && commit=${COMMIT} || commit=${current_commit}

ref_dir="../ref_test_results/${commit}"

echo "Will take reference results in ${ref_dir}"

[ -e ${ref_dir} ] || error_exit "No reference results found for ${commit}"

[ ${1} ] && support_type=${1} || support_type="bootstrap"
# support_type="posterior"

case ${support_type} in
    "bootstrap")
        suffix=".log"
        intrees="--intree ../test_data/synthesistree.tre"
        test_data_dir="../test_data/bootstrap_test_data"
        taxa_file="${test_data_dir}/taxons"
        gene_file="${test_data_dir}/marqueurs"
        root="Antilocapraamericana"
        wtset_name="Cumulated_bootstrap_proportions"
        do_compare="ref"
        ;;
    "posterior")
        suffix=".parts"
        intrees=""
        test_data_dir="../test_data/newformat_test_data"
        taxa_file="${test_data_dir}/taxa.txt"
        gene_file="${test_data_dir}/genes.txt"
        root="T000"
        wtset_name="Cumulated_posterior_probabilities"
        do_compare=""
        ;;
    *) error_exit "Unknown support type: ${support_type}";;
esac

mkdir -p paup
echo "begin paup;
    set autoclose=yes warntree=no warnreset=no;
    log start file=${support_type}_test_supermatrix_paup.log replace;" > paup/runpaup.nex

for py_version in "2.7" "3.6"
do
    if [ ${do_compare} ]
    then
        ref_matrix=${ref_dir}/${support_type}_test_supermatrix_${py_version}.nex
        [ -e ${ref_matrix} ] || error_exit "No reference results found for ${py_version}"
    fi
    echo "Running supertri.py with python${py_version}"
    python${py_version} ../supertri.py \
            --datasets ${gene_file} \
            --indir ${test_data_dir} \
            ${intrees} \
            -o ${support_type}_test_supermatrix.nex \
            --outdir . \
            --suffix ${suffix} \
            --taxlist ${taxa_file} \
            --root ${root} \
            > log_${support_type}_${py_version} \
            2> err_${support_type}_${py_version} || error_exit "supertri.py failed for ${support_type} and ${py_version}"
    if [ "${intrees}" ]
    then
        mv synthesistree.tre.withindices ${support_type}_test_synthesistree_${py_version}.tre
    fi
    mv ${support_type}_test_supermatrix.nex ${support_type}_test_supermatrix_${py_version}.nex
    if [ ${do_compare} ]
    then
        echo "Comparing supermatrices"
        supermatrix_ref_md5=$(tail -n +3 ${ref_matrix} | md5sum)
        supermatrix_md5=$(tail -n +3 ${support_type}_test_supermatrix_${py_version}.nex | md5sum)
        echo -e "md5 ${py_version} ref:\t${supermatrix_ref_md5}"
        echo -e "md5 ${py_version}:\t${supermatrix_md5}"
        tail -n +93 ${ref_matrix} \
            | awk '{print $1,$3}' | sort -n > wtset_${py_version}_ref.txt
        tail -n +93 ${support_type}_test_supermatrix_${py_version}.nex \
            | awk '{print $1,$3}' | sort -n > wtset_${py_version}.txt
        cp -f ${ref_matrix} paup/${support_type}_test_supermatrix_${py_version}_ref.nex
        cat >> paup/runpaup.nex <<PAUPBATCH
        execute ${support_type}_test_supermatrix_${py_version}_ref.nex;
        outgroup ${root};
        assume wtset=${wtset_name};
        hsearch;
        savetrees file=trees_${py_version}_ref.nex replace=yes;
PAUPBATCH
    fi
    cp -f ${support_type}_test_supermatrix_${py_version}.nex paup/${support_type}_test_supermatrix_${py_version}.nex
    cat >> paup/runpaup.nex <<PAUPBATCH
    execute ${support_type}_test_supermatrix_${py_version}.nex;
    outgroup ${root};
    assume wtset=${wtset_name};
    hsearch;
    savetrees file=trees_${py_version}.nex replace=yes;
PAUPBATCH

done

echo "    quit;
end;" >> paup/runpaup.nex

# Running paup on the supermatrices
# (paup can be obtained at http://phylosolutions.com/paup-test/)
echo "Running paup on the supermatrices"
( cd paup && paup runpaup.nex > paup.o 2> paup.e )
echo "Comparing resulting trees"
python3.6 check_paup.py ${do_compare}
