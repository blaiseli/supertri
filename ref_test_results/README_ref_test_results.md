Reference results generated using revision 57 as follows:
    python2.7 ../supertri.py \
        --datasets ../test_data/bootstrap_test_data/marqueurs \
        --indir ../test_data/bootstrap_test_data \
        --intree ../test_data/synthesistree.tre \
        -o bootstrap_test_supermatrix.nex \
        --outdir . \
        --suffix ".log" \
        --taxlist ../test_data/bootstrap_test_data/taxons \
        --root Antilocapraamericana
    mv synthesistree.tre.withindices bootstrap_test_synthesistree.tre

Updated reference results generated using commit 064ba82dfa2fdf5054d002416f43d61471bb4565 as follows:

    COMMIT="064ba82dfa2fdf5054d002416f43d61471bb4565" ./make_ref.sh

