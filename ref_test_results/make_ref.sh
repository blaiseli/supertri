#!/bin/bash
# Generate new reference results using the current state of the code.
# Currently, supertri.py has to be at the same state as in the latest commit.

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

diffs=$(git diff ../supertri.py | wc -l)
[ ${diffs} -eq 0 ] || error_exit "supertri.py has changed since last commit"


current_commit=$(git log | head -1 | cut -d " " -f 2)
# "commit name" can be forced using COMMIT environment variable
[ ${COMMIT} ] && commit=${COMMIT} || commit=${current_commit}

mkdir ${commit} || error_exit "cannot create directory ${commit}"
for py_version in "2.7" "3.6"
do
    python${py_version} ../supertri.py \
        --datasets ../test_data/bootstrap_test_data/marqueurs \
        --indir ../test_data/bootstrap_test_data \
        --intree ../test_data/synthesistree.tre \
        -o ${commit}/bootstrap_test_supermatrix.nex \
        --outdir ${commit} \
        --suffix ".log" \
        --taxlist ../test_data/bootstrap_test_data/taxons \
        --root Antilocapraamericana \
        > ${commit}/log_${py_version} \
        2> ${commit}/err_${py_version} \
        || error_exit "supertri failed with python${py_version}"
    mv ${commit}/synthesistree.tre.withindices ${commit}/bootstrap_test_synthesistree_${py_version}.tre
    mv ${commit}/bootstrap_test_supermatrix.nex ${commit}/bootstrap_test_supermatrix_${py_version}.nex
done

# Running paup on the supermatrices
# (paup can be obtained at http://phylosolutions.com/paup-test/)
mkdir -p ${commit}/paup
cp -f ${commit}/bootstrap_test_supermatrix_2.7.nex ${commit}/paup/bootstrap_test_supermatrix_2.7.nex
cp -f ${commit}/bootstrap_test_supermatrix_3.6.nex ${commit}/paup/bootstrap_test_supermatrix_3.6.nex
cat > ${commit}/paup/runpaup.nex <<PAUPBATCH
begin paup;
    set autoclose=yes warntree=no warnreset=no;
    log start file=bootstrap_test_supermatrix_paup.log replace;
    execute bootstrap_test_supermatrix_2.7.nex;
    outgroup Antilocapraamericana;
    assume wtset=Cumulated_bootstrap_proportions;
    hsearch;
    savetrees file=trees_27.nex replace=yes;
    execute bootstrap_test_supermatrix_3.6.nex;
    outgroup Antilocapraamericana;
    assume wtset=Cumulated_bootstrap_proportions;
    hsearch;
    savetrees file=trees_36.nex replace=yes;
    quit;
end;
PAUPBATCH
( cd ${commit}/paup && paup runpaup.nex > paup.o 2> paup.e )

exit 0
