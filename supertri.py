#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vi:et! autoindent tabstop=4 shiftwidth=4 smarttab expandtab
#
# supertri.py
#
# Copyright (C) 2007-2019 Anne Ropiquet, Blaise Li and Alexandre Hassanin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
from __future__ import print_function
"""
usage: supertri.py [options] <arguments>
available options:
    --help: prints this help text and exits
    --licence: prints information about the licence under which this program is released and exits
    --datasets <filename>: provides the name ot the file in which the names of the datasets used in the analyses are stored
    --indir <dirname>: provides the name of the directory in which the input datasets are located. By default, this program looks in the current working directory.
    --outdir <dirname>: provides the name of the directory in which to write the output files.
    --intree <filename>: provides the name of a nexus tree file containing trees on which the indices can be mapped
    -o <filename>: provides the name of the nexus file to write
    --root <taxon>: provides the name of the root for the output trees
    --suffix <.suf>: provides the suffix of the files containing the results to be read
    --taxlist <filename>: provides the name of the file in which the taxon names are stored
"""
__version__ = "$Revision: 60$"
__licence__ = """
Copyright (C) 2007-2018 Anne Ropiquet, Blaise Li and Alexandre Hassanin

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
"""

import copy
import getopt
import os
from errno import EEXIST
import re
import warnings
import sys

if sys.version_info < (3,):
    integer_types = (int, long,)
    ## debug
    def debug(msg):
        sys.stderr.write(re.sub(r"(\d+)L", r"\1", msg))
    ##
else:
    integer_types = (int,)
    ## debug
    def debug(msg):
        sys.stderr.write(re.sub("}", "]", re.sub("{", "[", msg)))
    ##

###################
# Small utilities #
###################


def mac2x(string):
    """mac2x(string) returns a string corresponding to <string> but with the mac-style ends of line translated to unix-style ends of line."""
    return re.sub("\r", "\n", string)

###########
# Objects #
###########

def most_specialized_class(obj1, obj2):
    """Return the most specialized class between those of objects
    *obj1* and *obj2*"""
    cl1 = obj1.__class__
    cl2 = obj2.__class__
    if isinstance(obj1, cl2):
        return cl1
    if isinstance(obj2, cl1):
        return cl2


class NodeIndex(float): # It's a float but with special printing format.
    """This object represents a node support index."""
    # This is a class attribute; when it is changed, it applies to all
    # instances of the class.
    format = "%.2f"
    def __init__(self, initval):
        float.__init__(initval)
    def __str__(self):
        return self.__class__.format % float(self)
    def reformat(self, newformat):
        """This function changes globally the format for all indexes of the same class as self."""
        assert isinstance(newformat, str), "newformat must be a string."
        self.__class__.format = newformat
    # Some operations on NodeIndex instances are redefined so as to produce objects of the same class instead of plain floats:
    def __add__(self, other):
        return most_specialized_class(self, other)(float(self) + float(other))
    def __sub__(self, other):
        return most_specialized_class(self, other)(float(self) - float(other))
    def __mul__(self, other):
        return most_specialized_class(self, other)(float(self) * float(other))
    def __div__(self, other):
        return most_specialized_class(self, other)(float(self) / float(other))
    def __radd__(self, other):
        return most_specialized_class(self, other)(float(other) + float(self))
    def __rsub__(self, other):
        return most_specialized_class(self, other)(float(other) - float(self))
    def __rmul__(self, other):
        return most_specialized_class(self, other)(float(other) * float(self))
    def __rdiv__(self, other):
        return most_specialized_class(self, other)(float(other) / float(self))
    # Python3 seems to ignore __div__
    def __truediv__(self, other):
        return self.__div__(other)
    def __rtruediv__(self, other):
        return self.__rdiv__(other)


class Bootstrap(NodeIndex):
    """This object represents a node bootstrap percentage."""
    format = "%.0f"
    def __init__(self, initval):
        NodeIndex.__init__(self, initval)
    def __str__(self):
        # Bootstrap values are displayed as percentages.
        return self.__class__.format % (100 * float(self))


class PosteriorP(NodeIndex):
    """This object represents a node posterior probability."""
    format = "%.2f"
    def __init__(self, initval):
        NodeIndex.__init__(self, initval)


class Bipart(object):
    """This object represents a bipartition."""
    def __init__(self, inclade, outclade, brlen=None):
        # key: dataset involved in the production of the Bipart
        # value: weight in this dataset
        self.weights = {}
        self.weight = NodeIndex(0)  # weight in the matrix representation; the sum of the support values for the different datasets
        ## Il faudrait self.indices, qui aurait ensuite les sous-attributs support, meansup, occurrences, repro, etc...
        ##self.support inutile ? (=self.weight, cf update())
        # sum of supports
        self.support = NodeIndex(0)
        # mean support
        self.meansup = NodeIndex(0)
        # number of datasets supporting the bipartition
        self.occurrences = 0
        # reproducibility
        self.repro = 0
        assert isinstance(inclade, integer_types) and isinstance(outclade, integer_types), "Sets of taxa should be described by integers."
        assert inclade & outclade == 0, "The two parts of a Bipart cannot have shared taxa."
        self.id = frozenset([inclade, outclade])
        self.inclade = inclade
        self.outclade = outclade
        # optional attribute that indicates the length of the branch separating the two parts of the bipartition
        self.brlen = brlen
        self.domain = inclade + outclade

    def __str__(self):
        return "Bipart(%s, %s)" % (self.inclade, self.outclade)
        #return str(self.id)

    def compatible(self, other):
        """self.compatible(other) returns True if self and other can be found in the same tree."""
        assert isinstance(other, (Bipart, Clade, list)), "Must be compared with a clade or a bipartition or a list of such objects."
        if isinstance(other, list):
            ## TODO: Issue a warning here ##
            return self.allcompat(other)
        assert self.domain == other.domain, "Cannot state about compatibility if the validity domains are not the same."
        if self.inclade == 0 or other.inclade == 0:
            return True
        a = self.inclade & other.inclade
        b = self.outclade & other.inclade
        c = self.inclade & other.outclade
        d = self.outclade & other.outclade
        if isinstance(self, Clade) and isinstance(other, Clade):
            return (a == 0 or b == 0 or c == 0)
        else:
            return (a == 0 or b == 0 or c == 0 or d == 0)
        ## TODO: Inserer la demonstration ici ##

    def allcompat(self, listofothers):
        """self.allcompat(listofothers) returns True if self is compatible with all the elements of the list <listofothers>."""
        assert isinstance(listofothers, list), "allcompat only works with lists."
        result = True
        i = 0
        while result:  # No need to continue once one incompatibility has been found.
        #while result and listofothers[i]:
            if i == len(listofothers):  # All members of the list have been tested.
                return result
            result = result and self.compatible(listofothers[i])
            i += 1
        return result

    def update(self, w, newdata=None):
        """self.update(w, newdata) increases the weight of the bipartition and records the new data that produced the bipatition."""
        # A NodeIndex object is also a float.
        assert isinstance(w, (float,) + integer_types) and w >= 0, "The weight is supposed to be a positive number."
        assert isinstance(newdata, list) or newdata is None, "If there is new data implied, it should be given as a list of datasets names."
        if newdata is not None:
            for m in newdata:
                if m in self.weights:
                    raise Exception("dataset %s already taken into account" % m)
                else:
                    self.weights[m] = w
                    # self.weights[m] = w + self.weights.get(m, 0)
        ## The order of the terms to be added might matter; the expression above needs to be re-checked.
        self.weight += w
        #adding in this order, self.weight should inherit the type of w
        self.support += w
        #adding in this order, self.support should inherit the type of w
        #if w > 0.5:
        #    self.occurrences = self.occurrences + 1

    def clade(self, root):
        """self.clade(root) returns a clade corresponding to the rooting of self by the taxon <root>, given as a power of 2."""
        if root & self.outclade == root:
            clad = Clade(self.inclade, self.outclade, root)
        elif root & self.inclade == root:
            clad = Clade(self.outclade, self.inclade, root)
        else:
            raise Exception("Cannot root with a taxon not placed inside neither outside the bipartition.")
        for attr in self.__dict__:
            if attr not in ['id', 'inclade', 'outclade', 'domain']:
                clad.__dict__[attr] = copy.deepcopy(self.__dict__[attr])
        return clad


class Clade(Bipart):
    """This object represents a clade, that is an oriented bipartition.
    It also has a dictionary of subclades, to optionally specify its internal topology."""
    def __init__(self, inclade, outclade, root=None, brlen=None):
        Bipart.__init__(self, inclade, outclade, brlen)
        # id is a tuple instead of a frozenset; this allows the distinction between a clade and its complementary.
        self.id = (self.inclade, self.outclade)
        assert (root is None) or (root & outclade == root), "If explicit, the root should be one of the taxa outside the clade."
        # optional attribute that indicates with respect to which taxon the clade is rooted
        self.root = root
        self.subclades = {} ## TODO: Peut-etre faudrait-il definir une sous-classe de Clade, avec la topologie definie au lieu d'ajouter cet attribut. Mais que mettre comme id a un tel clade ?

    def bipart(self):
        """self.bipart() returns a bipartition corresponding to the unrooting of self."""
        bip = Bipart(self.inclade, self.outclade, self.brlen)
        for attr in self.__dict__:
            if attr not in ['id', 'inclade', 'outclade', 'root', 'domain', 'subclades']:
                bip.__dict__[attr] = copy.deepcopy(self.__dict__[attr])
        return bip

    ## Beware of the implication of this method on the use of the "in" operator.
    def __contains__(self, other):
        """self.__contains__(other) returns True if Clade <other> can be a subclade of self, False otherwise.
        Empty clades are considered included in any clade that shares the same validity domain. Inclusion implies compatibility."""
        assert isinstance(other, Clade), "To state about inclusion, the object must be a Clade type object."
        if other.domain == self.domain:
            inter = other.inclade & self.inclade
            ## Verification
            if other.inclade == inter:
                assert self.compatible(other), "other is in self, so it should be compatible."
            return other.inclade == inter
        else:
            return False

    # def __cmp__(self, other):
    #     """self.__cmp__(other) is called when <, ==, or > are called to compare self and other.
    #     Based on this __cmp__ implementation, the order relation between clades with the same validity domain that are compatible
    #     will place clades included in other clades before these other clades."""
    #     if self.id == other.id:
    #         return 0  # Clades are equal if they have the same outside and the same inside
    #     if self.domain == other.domain:
    #         return cmp(self.inclade, other.inclade)
    #         #diff = self.inclade - other.inclade
    #         #if diff < 0:
    #         #    return -1
    #         #if diff > 0:
    #         #    return 1
    #         #elif diff == 0:
    #         #    return 0
    #         #else:
    #         #    raise Exception("A difference between integers should be either superior, equal, or inferior to zero.")
    #     else:
    #         raise ValueError("""To be compared, two clades must have the same validity domain,
    #                          that is, define a partition of the same set of taxa.""")

    def __eq__(self, other):
        if self.id == other.id:
            return True  # Clades are equal if they have the same outside and the same inside
        if self.domain == other.domain:
            return False
        else:
            raise ValueError("""To be compared, two clades must have the same validity domain,
                             that is, define a partition of the same set of taxa.""")

    def __ne__(self, other):
        if self.domain == other.domain:
            return self.id != other.id
        else:
            raise ValueError("""To be compared, two clades must have the same validity domain,
                             that is, define a partition of the same set of taxa.""")

    def __lt__(self, other):
        if self.domain == other.domain:
            return self.inclade < other.inclade
        else:
            raise ValueError("""To be compared, two clades must have the same validity domain,
                             that is, define a partition of the same set of taxa.""")

    def __le__(self, other):
        if self.domain == other.domain:
            return self.inclade <= other.inclade
        else:
            raise ValueError("""To be compared, two clades must have the same validity domain,
                             that is, define a partition of the same set of taxa.""")

    def __gt__(self, other):
        if self.domain == other.domain:
            return self.inclade > other.inclade
        else:
            raise ValueError("""To be compared, two clades must have the same validity domain,
                             that is, define a partition of the same set of taxa.""")

    def __ge__(self, other):
        if self.domain == other.domain:
            return self.inclade >= other.inclade
        else:
            raise ValueError("""To be compared, two clades must have the same validity domain,
                             that is, define a partition of the same set of taxa.""")

    def __str__(self):
        return "Clade(%s, %s)" % (self.inclade, self.outclade)

    def addsubclades(self, subclades):
        """self.addsubclades(subclades) recursively includes the clades in the list <subclades> into self.
        This method should not be called with a partial list of subclades,
        since all taxa of the clade not present in the subclades are considered being placed in an unresolved basal position (a "rake")."""
        assert isinstance(subclades, list), "This method only accepts a list of subclades as argument."
        # clades that can not be in the same tree as self
        rejected = []
        # selection of clades that could be included in self and should not be included somewhere else
        toinclude = []
        # selection of clades to propose for inclusion at the parent recursion level
        tokeep = []
        for s in subclades:
            assert isinstance(s, Clade), "The list should only comprise Clade type objects."
            assert s.domain == self.domain, "It doesn't make sense to combine clades with different validity domains."
            if s != self:
                if s in self: #cf __contains__ method
                    # A potential subclade
                    toinclude.append(s)
                elif self.compatible(s):
                    # Could go in an independent subtree
                    tokeep.append(s)
                else:
                    rejected.append(s)
        toinclude.sort()
        # There should only be clades that can be included in self in toinclude.
        # But it is not guaranteed that they are mutually compatible.
        # The order of priority is arbitrary; it is based on the values of the clades in sums of powers of 2.
        # Therefore, it should preferably be insured before calling self.addsubclades,
        # that the proposed subclades are mutually compatible.
        while toinclude:
            candidate = toinclude.pop()
            ## Utiliser allcompat ##
            possible = True
            for sub in self.subclades.values():
                if not sub.compatible(candidate):
                    # Once a clade is found to be not compatible with one already included,
                    # it should not be allowed to be included at a superior recursion level.
                    rejected.append(candidate)
                    possible = False
                    break
            if possible:
                self.subclades[candidate.id] = candidate
                # The remaining clades to include are proposed for inclusion in the new subclade.
                toinclude = self.subclades[candidate.id].addsubclades(toinclude)
                # The clades that have not been included are given back to the present level.
            else:
                warnings.warn("A clade that was not compatible with %s's subclades was found.\n" % self)
        # There may now remain terminal taxa that are not included in a subclade
        invalue = 0
        for s in self.subclades:
            invalue += self.subclades[s].inclade
        raketaxa = self.inclade - invalue # value of the taxa remaining to be included
        for t in decomposition(raketaxa): # The taxa are individually included in the clade.
            assert t != self.root, "The root cannot be included as a subclade."
            self.subclades[(t, self.domain - t)] = Clade(t, self.domain - t, self.root)
            ## il faudrait un truc qui homogénéise la classe du support dans un arbre.
            ## comme ca, par exemple mais c'est crade:
            #self.subclades[(t, self.domain - t)].support.__class__ = self.support.__class__
            #self.subclades[(t, self.domain - t)].weight.__class__ = self.weight.__class__
            ## ...et ca marche pas si on part d'un int...
            #else:
            #self.subclades[(t, self.domain - t)] = Clade(t, self.domain - t)
        return sorted(tokeep)

    def setsupporttype(self, type):
        """self.setsupporttype(type) recursively sets the support type to the class <self>."""
        self.support.__class__ = type
        for s in self.subclades.values():
            s.setsupporttype(type)

    def findclade(self, c):
        """self.findclade(c) tries to find if clade c is a subclade of self
        or of one of its subclades, and recursively.
        It returns a copy of the clade found, if there is one, None otherwise."""
        assert isinstance(c, Clade), "Can only tell inclusion for Clade objects."
        try:
            assert c.domain == self.domain, "Can only tell inclusion for clades with the same validity domain."
        except AssertionError:
            warnings.warn("Default for clades with different validity domains is to consider that one is not included in the other. Don't panic, this is just a little warning; the execution is probably going on...")
            return None
        if c ==  self:
            return copy.deepcopy(self)
        detected = None
        for s in self.subclades.values():
            if detected:
                return detected # no need to search further
            detected = s.findclade(c)
        return detected

    def issupportedby(self, tree):
        """self.issupportedby(tree) returns the support value of the clade of <tree> "equivalent" to the clade self if there is one, 0.0 if there is not, and None if this is not relevant.
        If there are missing taxa for <tree>, the support of a clade is actually the support of its restriction to the validity domain of <tree>.
        It is not relevant to assess the support when the minimal set of taxa necessary to "recognize" the clade self is missing from <tree>."""
        for s in self.subclades.values():
            if s.inclade & tree.domain == 0:
                # The tree is not relevant because it doesn't have the essential taxa;
                # at least one of its subclades has no possible equivalent in the restricted domain.
                return None
        supporting = tree.findclade(Clade(self.inclade & tree.domain, self.outclade & tree.domain))
        if supporting:
            return supporting.support
        return tree.support.__class__(0)

    ########################################################################
    ## Bug a arranger !!
    ########################################################################
    ##
    ## A revoir: le support moyen devrait prendre en compte aussi les clades qui ne sont pas dans les consensus majoritaires
    def computeindices(self, sourcetrees, bipartitions):
        """self.computeindices(sourcetrees) computes the mean support self.meansup and the reproducibility self.repro
        using the trees in the list <sourcetrees> and the bipartitions in <bipartitions>.
        It proceeds recursively for all subclades of self."""
        assert not (self.support or self.occurrences), "index computation must start from zero."
        #timesfound = 0 # Not used
        # All bipartitions are considered successively
        for b in bipartitions.iter_biparts():
            relevant = True # Is the bipartition compareable to self ?
            for s in self.subclades.values():
                if s.inclade & b.domain == 0:
                    # The bipartition is not relevant because it is from a tree
                    # that doesn't have the essential taxa; at least one of the
                    # subclades of self has no possible equivalent in the
                    # restricted domain.
                    relevant = False
                    break
            if relevant:
                # The relevant bipartitions are then examined more in detail
                if b.inclade == self.inclade & b.domain:
                    # The restriction to b.domain corresponds to the inner side of b
                    self.support = b.support + self.support # Count more support
                    #timesfound += len(b.weights) # Add the number of datasets where b was found
                elif b.outclade == self.inclade & b.domain:
                    # The restriction to b.domain corresponds to the outer side of b
                    self.support = b.support + self.support # Count more support
                    #timesfound += len(b.weights) # Add the number of datasets where b was found
        relevant = 0 # number of trees having enough taxa to be able to show an "equivalent" of self
        for t in sourcetrees:
            supp = self.issupportedby(t)
            if supp is not None:
                relevant += 1
                if supp:
                    self.occurrences += 1
        #assert timesfound >= self.occurrences
        #assert relevant >= timesfound
        self.repro = self.occurrences / float(relevant)
        self.meansup = self.support / float(relevant)
        for s in self.subclades:
            self.subclades[s].computeindices(sourcetrees, bipartitions)

    def parenth(self, converter, index = ""):
        """self.parenth(converter, index) returns the parenthesized description of self.
        <converter> is a dictionary that translates from powers of 2 to taxon names.
        If <index> is specified, the attribute whose name is <index> will be written as node labels."""
        assert isinstance(index, str), "If defined, index should be provided as a string."
        if self.inclade in converter: # If the clade is a terminal taxon, its name can be directly written.
            if self.brlen:
                return converter[self.inclade] + ":%s"%self.brlen
            return converter[self.inclade]
        #elif len(self.subclades) == 0:
        #    taxvalues = [] # list of the values of the taxa in self
        #    return "(" + ",".join(map(converter.get, taxvalues)) + ")"
        else:
            arguments = []
            for s in self.subclades.values():
                arguments.append((s, converter, index))
            if index:
                if self.brlen:
                    return "(" + ",".join([Clade.parenth(a[0], a[1], a[2]) for a in arguments]) + ")%s:%s" % (self.__dict__.get(index, ""), self.brlen)
                return "(" + ",".join([Clade.parenth(a[0], a[1], a[2]) for a in arguments]) + ")%s" % (self.__dict__.get(index, ""))
            if self.brlen:
                return "(" + ",".join([Clade.parenth(a[0], a[1], a[2]) for a in arguments]) + "):%s" % self.brlen
            return "(" + ",".join([Clade.parenth(a[0], a[1], a[2]) for a in arguments]) + ")"

    ## En cours ##
    def nextree(self, converter, index = "", toroot = True):
        """self.nextree(converter, index) returns the nexus representation of self.
        <converter> is a dictionary that translates from powers of 2 to taxon names.
        If <index> is specified, the attribute whose name is <index> will be written as node labels.
        If <toroot> is True, the root of the tree must be explicitly added to the nexus parenthesised form."""
        #if index == "repro":
        #    print self.__dict__[index]
        #    tmpfmt = self.__dict__[index].format
        #    self.__dict__[index].reformat("%s")
        if index == "meansup":
            # print(self.__dict__[index])
            tmpfmt = self.__dict__[index].format
            self.__dict__[index].reformat("%f")
        #else:
        #    tmpfmt = self.__dict__[index].format
        if not toroot:
            newick = "tree %s = (%s);" % (index, self.parenth(converter, index))
        else:
            newick = "tree %s = (%s,%s);" % (index, converter[self.root], self.parenth(converter, index))
        if index == "meansup":
            # restoring the default format
            self.__dict__[index].reformat(tmpfmt)
        return newick


class SetOfBiparts(object):
    """This object represents a set of bipartitions.
    It contains bipartitions of type Bipart.
    Bipartitions should be added using the method addbipart()"""
    __slots__ = ("_biparts", "_key_order", "tax2val")
    # __slots__ = ("_biparts", "tax2val")
    def __init__(self, converter, filenames, suffix=".parts"):
        msg = ("The converter is supposed to be a dictionary "
               "converting taxon names into powers of 2.")
        assert isinstance(converter, dict), msg
        # dictionary converting from taxon names to powers of 2
        self.tax2val = converter
        # keys are frozensets, values are bipartitions
        self._biparts = {}
        #for filename in filenames:
        #    os.path.basename(filename)
        self._key_order = None

    def predictable_key_order(self):
        """Generate a predictable order of the bipartition identifiers."""
        # return [frozenset(t) for t in sorted([tuple(sorted(k)) for k in self._biparts.keys()])]
        # Frozenset sorting may vary depending on the Python version.
        # Use tuples to determine the order.
        if self._key_order is None:
            self._key_order = [frozenset(t) for t in sorted([
                tuple(sorted(bipart_id)) for bipart_id in self._biparts])]
        for bip_id in self._key_order:
            yield bip_id
        # for t in sorted([
        #         tuple(sorted(bipart_id)) for bipart_id in self._biparts]):
        #     yield frozenset(t)

    def iter_biparts(self):
        # Not valid in python2.7
        # yield from self._biparts.values()
        for bipart in self._biparts.values():
            yield bipart

    def __contains__(self, bipart):
        return bipart in self._biparts

    # def __str__(self):
    #     name = "{"
    #     for b in self._biparts:
    #         name = name + self._biparts[b].__str__() + ","
    #     return name[:-1] + "}"

    def __str__(self):
        return "{%s}" % ",".join(map(str, self._biparts.values()))

    def read_biparts(self, filename, suffix, tstat=None):
        if suffix == ".parts":
            if tstat is None:
                msg = "Cannot guess tstat file name from %s." % filename
                assert filename[-5:] == "parts", msg
                tstat = filename[:-5] + "tstat"
            newbiparts = read_mb_biparts(filename, tstat)
        else:
            # Missing taxa have to be dealt with
            try:
                newbiparts = readbootlog(filename, len(self.tax2val))
            except:
                print("Is %s really a PAUP bootstrap log file ?\nTrying to read it as MrBayes .parts file." % filename)
                try:
                    newbiparts = readbiparts(filename)
                except:
                    print("For the moment, only PAUP log files or MrBayes .parts files are allowed.")
                    sys.exit(1)
        return newbiparts

    def addbipart(self, bip):
        """self.addbipart(bip) adds bipartition <bip> to self if it is new
        or updates self according to the new informations carried by <bip>."""
        msg = "Only Bipart objects can be added to a SetOfBiparts object."
        assert isinstance(bip, Bipart), msg
        if bip.id in self._biparts:
            self._biparts[bip.id].update(bip.weight, list(bip.weights))
        else:
            # self._key_order.append(bip.id)
            self._biparts[bip.id] = bip

    def matrixline(self, taxon, converter=None):
        """self.matrixline(taxon, converter) returns a nexus matrix representation line for taxon <taxon>
        with "1", "0", or "?" for the columns representing the bipartitions of self.
        "1" is when the taxon is present in the bipartition, 0 when it's outside and "?" when the information is missing.
        converter is the dictionary used to convert taxa to values as powers of 2."""
        assert isinstance(taxon, str), "Taxon names should be strings."
        # maxtaxname is global; it is the length of the longest taxon name, it is used to align the columns of the matrix correctly.
        def validname(taxon):
            """validname(taxon) returns the conversion of the name <taxon> to a string valid in PAUP.
            Spaces are replaced with underscores ("_") and if other non-alphanumeric characters are met,
            the string is placed between single quotes."""
            # True once special characters have been met
            special = False
            valid = ""
            for c in taxon:
                if c.isalnum():
                    valid = valid + c
                elif c.isspace() or c == "_":
                    valid = valid + "_"
                else:
                    valid = valid + c
                    special = True
            if special:
                valid = "'" + valid + "'"
            return valid
        line = validname(taxon) + (3 + maxtaxname - len(validname(taxon))) * " "
        if converter is None:
            converter = self.tax2val
        assert isinstance(converter, dict), "The converter is supposed to be a dictionary converting taxon names into powers of 2."
        val = converter[taxon]
        # ordering bipartition
        # should use the same when writing weight sets
        # can differ according to the implementation (python 2.7 vs 3.6 if keys are frozensets)
        # The difference disappears when keys are tuples
        #cols = sorted(self.keys())
        #for c in cols:
        #for c in self.keys():
        for c in self.predictable_key_order():
            ## debug
            try:
                _ = self._biparts[c]
            except KeyError as e:
                debug("%s not in \n%s" % (c, ", ".join(map(str, self._biparts))))
                raise
            ##
            for marker in self._biparts[c].weights:  # Multiplicate the columns.
                if self._biparts[c].inclade & val:
                    line = line + "1"
                elif self._biparts[c].outclade & val:
                    line = line + "0"
                else:
                    line = line + "?"
        return line

    def weightset(self, setname):
        """self.weightset() returns a nexus weight set corresponding to the weights of the bipartitions of self.
        *setname* is the name to give to the set."""
        assert isinstance(setname, str), "The name of a weight set should be a string."
        # ordering bipartition
        # should use the same when writing weight sets
        # can differ according to the implementation (python 2.7 vs 3.6 if keys are frozensets)
        # The difference disappears when keys are tuples
        #cols = sorted(self.keys())
        def weights(bipart_id):
            """weights(bipart_id) returns the list of the pairs (weight, marker)
            corresponding the bipartition whose identifier is <bipart_id>."""
            weights = [] # list of the weights
            for marker in self._biparts[bipart_id].weights:
                weights.append((self._biparts[bipart_id].weights[marker], marker))
            return weights
        wtset = "wtset %s = " % setname
        indent = " " * len(wtset)
        wtset = wtset + "\n"
        i = 1
        for bipart in self.predictable_key_order():
            for w in weights(bipart):
                wtset = wtset + indent + "%s: %s [%s],\n"%(w[0], i, w[1])
                i = i + 1
        # strip the indentation of the first weight and terminate the set by a semicolon:
        wtset = wtset[:len(indent)] + wtset[2*len(indent) + 1:-2] + ";\n"
        return wtset

    def nexmatrixrep(self, taxa, converter=None, weight=True, setname=None):
        """self.nexmatrixrep(weight, setname) returns the matrix representation of the bipartitions stored in self.
        <taxa> is the list of the taxon names that are supposed to be in the bipartitions.
        <converter> is the dictionary converting from taxon names to powers of 2.
        If <weight> is true, an assumptions bolck is added, defining the weights of the columns of the matrix.
        <setname> is the name to give to the weights set."""
        assert isinstance(taxa, list), "taxa should be a list of taxon names."
        if converter is None:
            converter = self.tax2val
        assert isinstance(converter, dict), "The converter is supposed to be a dictionary converting taxon names into powers of 2."
        if setname is None:
            setname = "weights"
        assert isinstance(setname, str), "The name of a weight set should be a string."
        nchar = 0
        for bip in self._biparts: # One bipartition may have several weights from different datasets.
            for marker in self._biparts[bip].weights:
                nchar += 1
        matrix = """#NEXUS
[produced by supertri.py %s]
begin data;
    dimensions ntax=%s nchar=%s;
    format datatype=standard missing=?;
    matrix
""" % (__version__, len(taxa), nchar)
        for t in taxa:
            matrix = matrix + self.matrixline(t, converter) + "\n"
        matrix = matrix + ";\nend;\n"
        if weight:
            matrix = matrix + "\nbegin assumptions;\n" + self.weightset(setname) + "end;"
        return matrix

    ## This is not the good way to do; it should depend on the tree on which the indexes will be reported. This is now done by a Clade method
    #def computeindices(self, ndatasets, trees):
    #    """self.computeindices(ndatasets, trees) calculates the mean support indices and the reproducibility indices of the bipartitions in self, simply by dividing their total support and number of occurrences by the number of datasets examined <ndatasets>.
    #    In order for the number of occurrences of the bipartitions to be counted, the trees in which to count should be provided in the list <trees>.
    #    This method should be called only after all datasets have been examined, so that their total support and their number of occurrences are up-to-date."""
    #    assert len(trees) == ndatasets, "For the moment, there seems to be no reason why the number of datasets should be different from the number of trees on which to count the occurrences of the bipartitions."
    #    for bip in self:
    #        self[bip].countoccurrences(trees)
    #        self[bip].setmeanindices(ndatasets)

###
# #
###

def decomposition(integer):
    """decomposition(integer) returns the list of the powers of 2 composing <integer>."""
    powers = []
    sum = 0
    puiss = 1
    while sum != integer:
        if puiss & integer:
            powers.append(puiss & integer)
            sum = sum + powers[-1]
        puiss = puiss * 2
    return powers


def read_taxa(taxfich):
    sys.stdout.write("\nTaxon names will be read in %s\n" % taxfich)
    if taxfich is not None:
        with open(taxfich, 'r') as fh:
            taxa = fh.read().split()
    else:
        print(__doc__)
        raise Exception("No taxon names provided. Either use option -i or provide a file with option --taxlist.")
    sys.stdout.write("The %s taxa that have been read are, in this order:\n" % len(taxa))
    sys.stdout.write(", ".join(taxa) + "\n")
    # Building dictionaries to convert taxon names to powers of 2 and vice-versa
    pow = 1
    # to convert from powers of 2 to names
    val2tax = {}
    # to convert from names to powers of 2
    tax2val = {}
    # used to align the supertree matrix
    for t in taxa:
        val2tax[pow] = t
        tax2val[t] = pow
        pow = 2 * pow
    # value of the set of all taxa
    totval = pow - 1
    # A taxon is uniquely defined by its value as a power of 2
    return (taxa, val2tax, tax2val, totval)

###
# #
###

def readbootlog(bootlog,ntaxs):
    """readbootlog(ntaxs) reads the PAUP bootstrap log file <bootlog> that contains the description of bipartitions in the form of "." and "*", with values associated to these bipartitions (for example, bootstrap supports).
    This function returns a list of pairs representing the bipartitions read in <bootlog>. and their associated values."""
    print("reading %s ..." % bootlog)
    with open(bootlog) as fh:
        lines = mac2x(fh.read()).split("\n")
    selection = []
    for line in lines:
        goodline = re.match("^[\.\*]+.*$",re.sub("\\[.*\\]", "", line))
        if goodline is not None:
            selection.append(goodline.group())
    ##
    bipparts = [] # list of lines describing the bipartitions, each line consists in a list of parts of the line
    partindex = 0
    i = len(selection) - 1 # index of the last line supposed to describe a bipartition
    reflen = len(selection[i].split()[0]) # length of the first field of the last line
    totlen = reflen # this will be the cumulated length of the fields supposed to describe a bipartition
    while len(selection[i].split()) >= 2: # This is a line with 2 fields; there is a bootstrap proportion after a blank.
        linelen = len(selection[i].split()[0])
        assert linelen == reflen, "The lines describing bipartitions are supposed to be of equal lengths if they are in the same block."
        bipparts.append([selection[i].split()[0], selection[i].split()[-1]]) # Two fields are added, the last beeing the bootstrap value.
        i = i - 1 # previous line in selection
        if i < 0: # The bipartitions were in one block.
            break # to avoid an IndexError
    # No more lines with 2 fields
    nbiparts = len(bipparts) # Number of bipartitions
    if i >= 0:
        bipindex = 0
        assert len(selection[i].split()) == 1, "Only the last block of the bipartitions description may have a support value following stars and points after a space."
        reflen = len(selection[i].split()[0])
        totlen = totlen + reflen
    while i >= 0: # There are parts of line to add.
        linelen = len(selection[i].split()[0])
        assert len(selection[i].split()) == 1, "Only the last block of the bipartitions description may have a support value following stars and points after a space."
        assert linelen == reflen, "The lines describing bipartitions are supposed to be of equal lengths if they are in the same block."
        bipparts[bipindex][0:0] = [selection[i].split()[0]] # The new part of line is inserted as first item of the list of parts of the line.
        i = i - 1 # previous line in selection
        if i < 0:
            break # to avoid an IndexError
        if bipindex == nbiparts - 1:
            if i >= 0: # There will be other parts of line to add.
                bipindex = 0
                assert len(selection[i].split()) == 1, "Only the last block of the bipartitions description may have a support value following stars and points after a space."
                reflen = len(selection[i].split()[0])
                totlen = totlen + reflen
        else:
            bipindex = bipindex + 1
    #There might be missing taxa, so this assertion is not relevant:
    #assert totlen == ntaxs, "The bipartitions should be described by as many characters as there are taxa."
    bipartitions = []
    for bipline in bipparts:
        bipartitions.append(("".join(bipline[:-1]), float(re.sub("\%", "", bipline[-1]))/100))
    bipartitions.reverse()
    return bipartitions


def read_mb_biparts_old(partsfile):
    """read_mb_biparts(partsfile) reads the file <partsfile> that contains the
    description of bipartitions in the form of "." and "*" as MrBayes does,
    with values associated to these bipartitions (typically posterior probabilities).
    This function returns a list of pairs representing the bipartitions read
    in <partsfile>. and their associated values."""
    bipartitions = []
    fich = open(partsfile,'r')
    lines = mac2x(fich.read()).split("\n")
    fich.close()
    for line in lines:
        # match1 = re.sub("\\[.*\\]","",line)
        # match2 = re.match("^.*\d+.*$",match1)
        bipline = re.match("^.*[\.\*]+.*$", re.sub("\\[.*\\]", "", line))
        if bipline is not None:
            fields = bipline.group().split()
            bipartitions.append((
                re.findall("[\.\*]+", bipline.group())[0],
                float(fields[2])))
    return bipartitions


def read_mb_biparts(parts, tstat):
    """Read bipartitions from the file *parts* and values associated to these
    bipartitions (typically posterior probabilities) in the *tstat* file.

    The description of bipartitions should be in the form of "." and "*"
    preceded by a bipartition ID. The posterior probabilities should be in the
    third column of the *tstat* file, and the bipartition ID in the first
    column. This file is tab-separated, and it seems that its first two columns
    are separated by a pair of tabs.

    This function returns a list of pairs representing the bipartitions read in
    *parts*. and their associated values.
    """

    probs = {}
    with open(tstat) as fh:
        _ = fh.readline()
        # Python 3 only
        # [col1, _, col3, *_] = fh.readline().split()
        header_cols = fh.readline().split()
        msg = "Unexpected .tstat format in %s" % tstat
        assert header_cols[0] == "ID", msg
        assert header_cols[2] == "Probability(=s)", msg
        assert len(header_cols) == 7, msg
        for line in fh:
            fields = re.sub("\\[.*\\]", "", line).split()
            parts_id = fields[0]
            prob = float(fields[2])
            msg = "Bipartition %s already recorded!" % parts_id
            assert parts_id not in probs, msg
            probs[parts_id] = prob 
    bipartitions = []
    with open(parts) as fh:
        _ = fh.readline()
        header = fh.readline()
        [col1, col2] = header.split()
        msg = "Unexpected .parts format in %s" % parts
        assert col1 == "ID" and col2 == "Partition", msg
        for line in fh:
            [parts_id, dotstars] = re.sub("\\[.*\\]", "", line).split()
            if dotstars.count("*") == 1 or dotstars.count(".") == 1:
                # It's a trivial bipartition
                continue
            bipartitions.append((dotstars, probs[parts_id]))
    return bipartitions


def valstars(line, missingvals=None):
    """valstars(line, missing) reads a line <line> composed of "." and "*" representing a bipartition.
    This function returns the value of the set of taxa represented by a "*" and the value of the set of taxa represented by a ".", assuming that taxa whose values are in the list <missingvals> are not represented in <line>."""
    if missingvals is None:
        missingvals = {}
    # Value representing a taxon, as a power of 2
    taxval = 1
    val = 0 
    compl = 0
    for symbol in line:
        if symbol == "?":
            raise Exception("Question marks are not authorized yet. "
                            "Missing taxa must be missing for all "
                            "bipartitions produced by the dataset "
                            "and be declared in a .abs file.")
        msg = ("A taxon must be in or out of the bipartition, "
               "or be declared as missing from the dataset in a .abs file.")
        assert symbol in ["*", "."], msg
        # skipping missing taxa
        while taxval in missingvals:
            taxval *= 2
        if symbol == "*":
            val += taxval
        else:
            compl += taxval
        taxval *= 2
    return val, compl


def readnextrees(treefile, converter):
    """readnextrees(file, converter) reads the trees in nexus format that are written in the file <treefile>.
    These trees must use only taxa that are keys of the dictionary <converter>.
    <converter> is a dictionary that translates from taxon names to powers of 2.
    This function returns a list of Clade objects that correspond to the trees, without a specified root."""
    trees = []
    with open(treefile, 'r') as fh:
        for line in map(mac2x, fh):
            treeline = re.match(".*tree.*\\(.*\\);", re.sub("\\[.*\\]", "", line))  # Comments are removed from the line.
            if treeline is not None:
                parenth = re.sub("\\);[^()]*$", ")", re.sub("^[^()]*\\(", "(", treeline.group()))
                domain = 0
                for t in converter:
                    if t in parenth:
                        domain = domain + converter[t]
                trees.append(readparenth(parenth, converter, domain))
    return trees


def regtaxname(t):
    """regtaxname(t) returns a compiled regular expression that should match <t> as a taxon name in a newick tree, that is <t> between separators like ":", "(", "," or ")"."""
    # The taxon name should be between 2 non-word characters.
    reg = re.compile("\\W%s\\W" % t)
    return reg


def readparenth(parenth, converter, domain, root=None, brlen=None):
    """readparenth(parenth, converter, domain, root, brlen) builds the Clade object that corresponds to the newick format tree <parenth>. This tree must only use taxa that are keys in the dictionary <converter>.
    <converter> is a dictionary that translates from taxon names to powers of 2.
    <domain> is the sum of the values of the taxa on which the clade is defined; its validity domain.
    If <root> is specified, it will be used as explicit root for the clade.
    If <brlen> is provided, it will be recorded in the Clade object."""
    #intaxa = [] # list of the taxa that are in the clade
    assert parenth[0] == "(" and parenth[-1] == ")", "A newick clade must be delimited by parentheses."
    if root is not None:
        assert root & domain, "The root must be in the validity domain."
    subclades = [] # list of first-level subclades to include in the clade
    inclade = 0
    for t in converter:
        r = regtaxname(t)
        if r.search(parenth) is not None:
        #if t in parenth: #source de bug; utiliser expressions regulieres.
            inclade = inclade + converter[t]
    clade = Clade(inclade, domain - inclade, root, brlen)
    parlevel = 1
    i = 0
    while parlevel != 0:
        i = i + 1
        if parlevel == 1 and parenth[i] not in ["(", ")", ","]: # A first-level terminal taxon is begining.
            termval, brlen, i = readterminal(parenth, i, converter)
            if termval is not None:
                subclades.append(Clade(termval, domain - termval, root, brlen))
        if parenth[i] == "(":
            parlevel += 1
            if parlevel == 2: # A first-level sub clade has just been opened.
                ibegin = i
        if parenth[i] == ")":
            parlevel -= 1
            if parlevel == 1: # A first-level subclade has just been closed.
                iclose = i
                subparenth = parenth[ibegin:iclose+1]
                brlen, i = readbrlen(parenth, i)# override brlen with value for new subclade
                subclades.append(readparenth(subparenth, converter, domain, root, brlen))
    clade.addsubclades(subclades)
    return clade


def readterminal(parenth, i, converter):
    """readterminal(parenth, i, converter) reads a terminal taxon and its possible branch length in the parenthesized clade <parenth> starting at positin <i> in this string.
    This function returns the value of the terminal taxon as obtained by the use of the dictionary <converter>, the value of the branch length if there is one, or None, and the position in <parenth> corresponding to the last character of the taxon or of its branch length."""
    r = re.compile("\w+:*\d*\.*\d*") # pattern matching a taxon name possibly followed by a branch length
    #r = re.compile("\w+\(*\d*\)*:*\d*\.*\d*") # pattern matching a taxon name followed by a branch length
    m = r.match(parenth, i) # use match rather than search: search tilts too soon
    if m is not None:
        pair = m.group().split(":")
        if len(pair) == 2:
            return converter[pair[0]], float(pair[1]), m.end()
        else:
            return converter[pair[0]], None, m.end()
    else:
        return None, None, i


def readbrlen(parenth, i):
    """readbrlen(parenth, i) returns the value of the branch length written in the parenthesized clade description <parenth> just after position <i> in this string. If there is no branch length, it return None.
    This function also retruns the position in <parenth> where the branch length ends."""
    if i+1 < len(parenth):
        if parenth[i+1] in [",", ")"]:
            return None, i
        if parenth[i+1] == ":":
            m = re.search(":\d*\.*\d*",parenth[i+1:])
            assert m is not None, "In a newick tree, a colon should be followed by a branch length value."
            return float(m.group()[1:]), i + m.end()
        raise NotImplementedError("This program can not read trees with node labels or similar ornementation.")
    return None, i


def getroot(converter, missingvalues = []):
    """getroot(converter, missingvalue) returns the name of an Arbitrarily chosen root taxon.
    <converter> is a dictionary translating from taxon names to powers of 2.
    <missingvalues> is a list of the values of the taxa that cannot be chosen as root."""
    assert isinstance(converter, dict), "converter should be a dictionary converting from taxon names to powers of 2."
    root = False
    while not root:
        for (taxon, taxval) in converter.items():
            if taxval not in missingvalues:
                root = taxon
                warnings.warn("root arbitrarily set to %s\n" % root)
    if not root:
        warnings.warn("All taxa seem to be missing; no valid root can be chosen.")
        sys.exit(1)
    return root


# TODO: modernize this
def sortclades(clades, attr):
    """sortclades(clades, attr) returns the sorted list corresponding to the list <clades>, sorted according to the attribute <attr> of the clades in the list. The clades with the highest value of attr are placed on top of the list."""
    # implementation taken from module func2 of package p4
    def pairing(clade, attr=attr):
        """pairing(clade, attr) returns the pair clade.attr, attr."""
        return (getattr(clade, attr), clade)
    pairs = list(map(pairing, clades))
    pairs.sort()
    def strip(pair):
        """strip(pair) returns the second element of a pair."""
        return pair[1]
    return list(map(strip, pairs))


def consensusclades(clades, type):
    """consensusclades(clades, type) returns a selection of the clades in list <clades>.
    The selection is made to obtain a consensus of the type specified by <type>."""
    ## Is it really a good idea to use the weight ? Why not the support ?
    sortedclades = sortclades(clades, "weight")
    sortedclades.reverse() # The clades with the highest weight come first.
    selection = []
    i = 0
    while sortedclades[i].weight == 1:
        selection.append(sortedclades[i])
        i += 1
        if i == len(sortedclades):
            break
    if type == "strict":
        return selection# else try adding more clades to the selection
    if i < len(sortedclades):
        while sortedclades[i].weight > 0.5:
            selection.append(sortedclades[i])
            i += 1
            if i == len(clades):
                break
    if type == "majrule":
        return selection
    if i < len(sortedclades):
        while sortedclades[i]:
            if sortedclades[i].allcompat(selection): # If clades with the same weight are not compatible, this greedy consensus arbitrarily selects the clade that appears first; the result is thus dependent on the way the sort method works.
                selection.append(sortedclades[i])
            i += 1
            if i == len(sortedclades):
                break
    if type == "greedy":
        return selection
    else:
        raise NotImplementedError("%s consensus not implemented.\nPossible types are strict, majrule and greedy.")%type

################
# Main program #
################

def main():
    info_head = """
    SuperTRI
    %s

    A Python script to help assembling supertree matrices with clade weights.

    Distributed under the GNU General Public Licence.
    """ % __version__[1:-1]
    print(info_head)
    # sys.stdout.write("\nSuperTRI\n%s\n\nA python script to help assembling supertree matrices with clade weights\n\nDistributed under the GNU General Public Licence\n\n" % __version__[1:-1])
    # Try to read options and arguments
    try:
        opts, args = getopt.getopt(sys.argv[1:], "o:", [
            "help", "licence", "indir=", "outdir=", "datasets=",
            "intree=", "root=", "suffix=", "taxlist="])
    except getopt.GetoptError:
        print(__doc__)
        sys.exit(1)
    greedy = True # Is the support evaluated using greedy consensuses (if False, it is done on simple majority rule trees) ?
    indir = "."
    outdir = None
    intreefiles = [] # list of treefile names given by the user to put the indexes on them
    markfich = None
    outfich = None
    root_name = None # tentative root name
    suffix = None
    taxfich = None
    for o, a in opts:
        if o == "--help":
            print(__doc__)
            sys.exit()
        if o == "--licence":
            print(__licence__)
            sys.exit()
        if o == "--indir":
            indir = a
        if o == "--outdir":
            outdir = a
        if o == "--datasets":
            markfich = a
        if o == "-o":
            outfich = a
        if o == "--intree":
            intreefiles.append(a)
        if o == "--root":
            root_name = a
        if o == "--suffix":
            suffix = a
        if o == "--taxlist":
            taxfich = a
    # available files:
    files = os.listdir(indir)
    toopen = {}
    # TODO: possible bug revealed by differences between py27 and py36
    # (likely related to dict ordering diffrences)?
    # Results seem sensitive to the order in which the files are read.
    marks = set() ## Why use a set instead of a list ?
    if markfich:
        fich = open(markfich,'r')
        for m in fich.read().split():
            marks.add(m)
        #marks = fich.read().split()
        fich.close()
        nmarks = len(marks)
    elif len(marks) == 0:
        print(__doc__)
        raise Exception("No dataset names provided. Provide a file with option --datasets.")
    sys.stdout.write("\nThere are %s markers:\n" % nmarks)
    sys.stdout.write(", ".join(marks) + "\n")
    try:
        if suffix[0] != ".":
            suffix = "." + suffix
    except:
        print("No file extension specified. Defaulting to MrBayes .parts file.")
        suffix = ".parts"
    # keys are marker names, values are names of files
    # listing missing taxa for the corresponding marker
    absfiles = {}
    # Extract those file names that match a marker and have the correct suffix
    for filename in files:
        if filename.endswith(suffix):
            for m in marks:
                if filename == m + suffix:
                    toopen[m] = os.path.join(indir, filename)
                    if m + ".abs" in files:
                        absfiles[m] = "%s.abs" % m
                        sys.stdout.write("%s was found. Expecting to find the names of the taxa missing for dataset %s in it.\n" % (absfiles[m], m))
                    break
    global maxtaxname
    (taxa, val2tax, tax2val, totval) = read_taxa(taxfich)
    maxtaxname = max(map(len, taxa))
    print("\nThe files in which the bipartions and their weights will be read are:")
    print("\n".join(toopen.values()))
    # dictionary whose keys are datasets names and the associated values are the trees produced by the dataset
    datasetsdict = {}
    B = SetOfBiparts(tax2val, toopen, suffix)
    # Loop over the files (and hence, the markers):
    for (m, fich) in toopen.items():
        ##needs suffix, taxa, fich, marks, root_name, tax2val, greedy
        ##local ? newbiparts, absfiles, datamarkers, clades, majclades, root, missingvalues, missingtot, majruletree
        ##modifies B, datasetsdict
        ##returns
        # When suffix is .parts, the name of the tstat file is inferred from the name of the parts file
        newbiparts = B.read_biparts(fich, suffix)
        # List of all the clades found for the current dataset
        clades = []
        # List of clades found in the majority of profile
        majclades = []
        root = False  # The rooting should be re-checked for each dataset, because the missing taxa might change.
        if m in absfiles:  # There are missing taxa.
            with open(os.path.join(indir, absfiles[m]), 'r') as fh:
                # list of the values corresponding to the taxa read in absfiles[m]
                #missingvalues = list(map(tax2val.get, fh.read().split()))
                missingvalues = [tax2val[taxname.strip()] for taxname in fh]
        else:
            missingvalues = []
        # root_name is the root proposed by the user.
        if root_name in tax2val and tax2val[root_name] not in missingvalues:
            root = root_name
        if not root:
            root = getroot(tax2val, missingvalues)
        # After defining the root, a tree is initiated with the "total clade":
        # the one comprising all the taxa that are not missing:
        # total value of the missing taxa
        missingtot = sum(missingvalues)
        # The tree must be defined on a domain not comprising the missing values.
        # The sum of the missing values is therefore substracted from the tentative outclade value.
        majruletree = Clade(
            totval - (tax2val[root] + missingtot),
            tax2val[root],
            root=tax2val[root])
        # Set the support type:
        if suffix == ".log":
            majruletree.weight = Bootstrap(0)
            majruletree.support = Bootstrap(0)
        elif suffix == ".parts":
            majruletree.weight = PosteriorP(0)
            majruletree.support = PosteriorP(0)
        else:
            majruletree.weight = NodeIndex(0)
            majruletree.support = NodeIndex(0)
        # Loop over bipartitions read in the file:
        for b in newbiparts:
            (inclade, outclade) = valstars(b[0], missingvalues)
            ## debug OK
            #debug("%s, %s\n" % (inclade, outclade))
            ##
            bip = Bipart(inclade, outclade)
            if suffix == ".log":
                # The bootstrap proportion for bip is set as its support and as its weight.
                bip.update(Bootstrap(b[1]), [m])
            elif suffix == ".parts":
                # The posterior probability for bip is set as its support and as its weight.
                bip.update(PosteriorP(b[1]), [m])
            else:
                bip.update(NodeIndex(b[1]), [m])
            ## test version: majority rule
            # The clade that is made inherits from bip's bootstrap proportion.
            clades.append(bip.clade(tax2val[root]))
            assert bip.weight == clades[-1].weight, "The clade has not the same bootstrap value as his mother bipartition. This should not happen."
            # Majority-rule selection:
            if bip.weight > 0.5:
                majclades.append(bip.clade(tax2val[root])) ## Faut-il faire une copie ici ? Non: bip.clade() se charge de produire un nouvel objet
            # If B already contains bip, the existing record is updated.
            ## debug
            debug(bip.weight.format)
            ##
            B.addbipart(bip)
        clades.sort()
        # majority-rule consensus; it contains only the clades appearing in more than half the profile
        majclades.sort()
        if greedy:  # majclades is overriden, it was there for a test version
            majclades = consensusclades(clades, "greedy")
        # Building the consensus tree:
        majruletree.addsubclades(majclades)
        datasetsdict[m] = majruletree
    # datasetsdict now contains a consensus tree for each marker
    # and B contains all bipartitions found in the log files, with their cumulated support.
    ## debug
    # sys.stderr.write("\n".join(["%s" % fzs for fzs in sorted(B.keys())]) + "\n")
    ##
    # To avoid truncated weights, indices are temporarily reformatted for the writing of the nexus matrix.
    if suffix == ".parts":
        tmpfmt = PosteriorP(0).format
        PosteriorP(0).reformat("%s")
        matrix = B.nexmatrixrep(taxa, setname="Cumulated_posterior_probabilities")
        PosteriorP(0).reformat(tmpfmt)
    elif suffix == ".log":
        tmpfmt = Bootstrap(0).format
        ## debug
        debug("before: %s\n" % tmpfmt)
        ##
        Bootstrap(0).reformat("%s")
        ## debug
        debug("after: %s\n" % Bootstrap(0).format)
        ##
        matrix = B.nexmatrixrep(taxa, setname="Cumulated_bootstrap_proportions")
        Bootstrap(0).reformat(tmpfmt)
    else:
        tmpfmt = NodeIndex.format
        NodeIndex(0).reformat("%s")
        matrix = B.nexmatrixrep(taxa)
        NodeIndex(0).reformat(tmpfmt)
    #sys.stdout.write("Here is the matrix that will be written:\n")
    #sys.stdout.write(matrix)
    try:
        fich = open(outfich, 'w')
        fich.write(matrix)
        fich.close()
        sys.stdout.write("\nFile %s written\n" % outfich)
    except:
        if outdir is None:
            matrix_outdir = indir
        else:
            matrix_outdir = outdir
        filename = os.path.join(matrix_outdir, "".join(marks) + (suffix == ".parts") * ".mrpp" + (suffix == ".log") * ".mrbp" + ".nex")
        sys.stdout.write("\nThe matrix will be written in file %s\n" % filename)
        with open(filename, 'w') as fh:
            fh.write(matrix)
    
###########################################################################
# Reading trees, calculating the indices, and reporting them on the trees #
###########################################################################
    
    for treefile in intreefiles:
        if outdir is None:
            tree_outdir = os.path.dirname(treefile)
        else:
            tree_outdir = outdir
        # https://stackoverflow.com/q/45283093/1878788
        try:
            os.makedirs(tree_outdir)
        except OSError as e:
            if e.errno != EEXIST:
                raise
        outfilename = os.path.join(tree_outdir, os.path.basename(treefile) + ".withindices")
        output = "#Nexus\n\n"
        newtrees = readnextrees(treefile, tax2val)
        for tree in newtrees:
            if suffix == ".log":
                tree.setsupporttype(Bootstrap(0).__class__)
            elif suffix == ".parts":
                tree.setsupporttype(PosteriorP(0).__class__)
            else:
                tree.setsupporttype(NodeIndex(0).__class__)
            # compute the indices using the consensus trees and the bipartitions
            tree.computeindices(datasetsdict.values(), B)
            output = output + "begin trees;\n"
            output = output + tree.nextree(val2tax, "meansup", False) + "\n"
            output = output + "end;\n"
            output = output + "begin trees;\n"
            output = output + tree.nextree(val2tax, "repro", False) + "\n"
            output = output + "end;\n"
        sys.stdout.write(output)
        with open(outfilename, 'w') as fh:
            fh.write(output)
        sys.stdout.write("\nThese trees have been written in %s\n" % outfilename)
    return 0

if __name__ == "__main__":
    sys.exit(main())
